import React from 'react'
import { Link, Outlet } from 'react-router-dom'
import Logo from '../assets/logo.png'
const RootLayout = () => {
    return (
        <div>
            <nav>
                <div className='container'>
                    <div className='flex items-center justify-center py-2'>
                        <div className='flex gap-10 bg-[#9a304c] pr-10 py-3 pl-6 rounded-tl-md rounded-bl-md text-white mr-[-7px]'>
                            <Link to={'/'} className='hover:text-gray-300 duration-200'>
                                Bosh sahifa
                            </Link>
                            <Link to={'about-us'} className='hover:text-gray-300 duration-200'>
                                Biz haqimizda
                            </Link>
                            <Link to={'send-material'} className='hover:text-gray-300 duration-200'>
                                Material yuborish
                            </Link>
                        </div>

                        <div className=' border-red-50 rounded-full mr-[-2px] z-[10]'>
                            <Link to={'/'}>
                                <img className='rounded-full w-60' src={Logo} alt='logo' />
                            </Link>
                        </div>

                        <div className='flex gap-10 bg-[#9a304c] pl-10 text-white py-3 pr-6 rounded-tr-md rounded-br-md ml-[-2px]'>
                            <Link to={'redaction'} className='hover:text-gray-300 duration-200'>
                                Redaktsia
                            </Link>
                            <Link to={'journals-articles'} className='hover:text-gray-300 duration-200'>
                                Jurnal va maqolalar
                            </Link>
                            <Link to={'contact'} className='hover:text-gray-300 duration-200'>
                                Bog'lanish
                            </Link>
                        </div>
                    </div>
                </div>
            </nav>
            <Outlet />
        </div>
    )
}

export default RootLayout