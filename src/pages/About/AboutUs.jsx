import React, { useState,useEffect } from 'react'
import { useTranslation } from "react-i18next";
import Footer from '../../components/Footer';
// import "./style.scss";
const AboutUs = () => {
  const [scroll, setScroll] = useState(0);
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      window.addEventListener("scroll", () => {
        if (window.scrollY - entry.target.offsetTop < 2 * window.innerWidth) {
          setScroll(
            window.scrollY -
            entry.target.offsetTop +
            (window.innerHeight * 2) / 3
          );
        }
      });
    });
  });

  useEffect(() => {
    const parent = document.getElementById("about");
    observer.observe(parent);
  }, []);

  return (
    <>
    <div className="about">
      <h1 className="about-title  m-0 p-0 font-bold font-sans text-center text-3xl text-white">Biz haqimizda</h1>
      <div className="AboutHome m-0 p-0 w-full h-full flex overflow-hidden relative items-center justify-center" id="about">
        <div className='container'>
        <div className="wrapper flex items-center justify-center pb-[300px]">
          <div className="col-12 text-card pb-0 px-[5vw] pt-[50px]">
            <div className="body py-[60px] px-[40px] pr-[50%] rounded-[15px] bg-[#fdeaea]">
              <p className="desc">Research in Adult Learning and Education” (RALE) jurnali hayot davomida ta’lim bilan shug‘ullanuvchi, mehnat bozori talablarini hisobga olgan holda kattalar uchun ta’lim va kasbga o’qitish sifatini ta’minlash bilan shug‘ullanuvchi o‘zbekistonlik va jahon amaliyotchilari va tadqiqotchilari uchun mo‘ljallangan nashrdir. Biz hayot davomida ta’limni rag‘batlantirish va odamlarning doimiy o‘zgarib borayotgan jamiyatda rivojlanish imkoniyatlarini kengaytirish tarafdorimiz. Biz kattalar ta'limining ahamiyatini odamlarni nafaqat tadqiqot va malakaga asoslangan muhim ko'nikmalar bilan qurollantiradigan, balki shaxsiy o'sish, ijtimoiy inklyuziya va iqtisodiy rivojlanishga yordam beradigan o'zgartiruvchi kuch sifatida tan olamiz. Jurnal kattalarning jamiyat va mehnatdagi ishtirokini rag'batlantirishga qaratilgan formal va noformal ta'lim va ta'limni yoritadi. Jurnalimizning har bir soni quyidagi va shunga o‘xshash sohalarda keng ko‘lamli mavzularni o‘z ichiga oladi: o kasb-hunar ta’limi pedagogikasi va psixologiyasi; o ijtimoiy sheriklikni rivojlantirish; o pedagogik va boshqaruv kadrlarining malakasini oshirish va qayta tayyorlash; o kattalar uchun ta'lim; o ish joyida o'qitish; o kasbiy rivojlanish. RALED butun dunyo bo'ylab kattalar ta'limi va ta'limining barcha jihatlarini o'rgatish va tadbiq etish yo'llarini boshqaradigan tamoyillar va amaliyotlarni konstruktiv muhokama qilish uchun muhit yaratishga intiladi. Shuningdek, u butun dunyo bo'ylab amaliyotchilar va tadqiqotchilar o'rtasida ma'lumot va g'oyalar almashinuvi uchun forumni taqdim etadi. Bizning taniqli tadqiqotchilar, o'qituvchilar va amaliyotchilar jamoasi kattalar ta'limining ko'p qirrali jihatlarini yoritishga ishtiyoqlidir. Izlanishlar, dalillarga asoslangan tadqiqotlar va tahlillar orqali biz kattalar ta'limi sohasidagi ilm va amaliyotni xabardor qilishga hissa qo'shishni maqsad qilganmiz, shuningdek, kattalar ta'limining global tabiatini va o’ziga xos masalalarini aks ettiruvchi turli istiqbollar va fanlararo tadqiqotlarni taqdim etishni maqsad qilganmiz. Tadqiqotchi sifatida siz kattalar ta'limini o'rganishga qaratilgan maqolalar, tadqiqot ishlari, amaliy tadqiqotlar, intervyular va sharhlarni kutishingiz mumkin. Biz sizni ushbu intellektual sayohatda bizga qo'shilishga taklif qilamiz, chunki biz ilg'or tadqiqotlar bilan shug'ullanamiz va kattalar o'quvchilarining hayotini boyitadigan va ushbu sohani rivojlantirishga hissa qo'shadigan amaliy natijalarni o'rganamiz. Biz butun dunyo bo'ylab tadqiqotchilar, o'qituvchilar, siyosatchilar va amaliyotchilarni jurnalimizga o'z tajribalari va g'oyalarini qo'shishga undaymiz. Tadqiqotlaringiz, innovatsion amaliyotlaringiz va tanqidiy mulohazalaringizni baham ko‘rish orqali siz jamoaviy bilimga hissa qo‘shishingiz va kattalar ta’limini yaxshilashga intilayotgan faol olimlar va amaliyotchilar hamjamiyatini rivojlantirishingiz mumkin. Bilim va o'zgarish uchun biz bilan birga bo'lganlarga rahmat. Keling, kattalar uchun ta'limning qiziqarli sohasini birgalikda o'rganaylik va yanada inklyuziv, adolatli va ma'rifatli jamiyat yaratishga harakat qilaylik. Buyurtmangizni yuboring XXXX yoki XXXX orqali biz bilan bog'laning</p>
            </div>
            <div className="col-5 person-card">
              <div className="youtube">
                {/* <img src={evt?.image} alt="" /> */}
              </div>
            </div>
          </div>
        </div>
          </div>
        <div className="tape">
          <p style={{ marginLeft: `-${scroll}px` }}>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> * <p>Raled</p>{" "}
          <p>Raled</p> <p>Raled</p> *
        </div>
        <div className="crimson">
          <p>Raled</p> * <p>Raled</p>
        </div>
        <div className="tape1">
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p> <p>Raled</p> *{" "}
          <p>Raled</p> <p>Raled</p>{" "}
          <p style={{ marginLeft: `-${scroll}px` }}>Raled</p> *
        </div>
        <div className="grey">
          <p>Raled</p> * <p>Raled</p>
      </div>
      </div>
    </div>
    <Footer/>
    </>
  )
}

export default AboutUs