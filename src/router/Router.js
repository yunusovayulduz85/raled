import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import RootLayout from '../layout/RootLayout'
import Home from '../pages/Home'
import AboutUs from '../pages/About/AboutUs'
import SendMaterial from '../pages/SendMaterial'
import Redaction from '../pages/Redaction'
import JournalsArticles from '../pages/JournalsArticles'
import Contact from '../pages/Contact'

const Router = () => {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<RootLayout />}>
                        <Route index element={<Home />} />
                        <Route path='about-us' element={<AboutUs />} />
                        <Route path='send-material' element={<SendMaterial />} />
                        <Route path='redaction' element={<Redaction />} />
                        <Route path='journals-articles' element={<JournalsArticles />} />
                        <Route path='contact' element={<Contact />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </div>
    )
}

export default Router